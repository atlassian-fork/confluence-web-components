import { expect } from 'chai';

import { convertRelativeToAbsolute } from '../src/html';

describe('HTML module', () => {
    describe('Relative url conversion', () => {
        it('should not modify content without links', () => {
            const html = '<div><a>Blah</a></div>';
            const absoluteHtml = convertRelativeToAbsolute(html, 'http://localhost:1990/');
            expect(absoluteHtml).to.equal(html);
        });

        it('should replace relative links with no leading /', () => {
            const html = '<div><a href="confluence">Blah</a></div>';
            const absoluteHtml = convertRelativeToAbsolute(html, 'http://localhost:1990/');
            expect(absoluteHtml).to.equal('<div><a href="http://localhost:1990/confluence">Blah</a></div>');
        });

        it('should replace relative links that contain a leading /', () => {
            const html = '<div><a href="/confluence">Blah</a></div>';
            const absoluteHtml = convertRelativeToAbsolute(html, 'http://localhost:1990');
            expect(absoluteHtml).to.equal('<div><a href="http://localhost:1990/confluence">Blah</a></div>');
        });

        it('should replace image paths with no leading /', () => {
            const html = '<div><img src="/myimage.gif" /></div>';
            const absoluteHtml = convertRelativeToAbsolute(html, 'http://localhost:1990');
            expect(absoluteHtml).to.equal('<div><img src="http://localhost:1990/myimage.gif" /></div>');
        });

        it('should replace image paths that contain a leading /', () => {
            const html = '<div><img src="myimage.gif" /></div>';
            const absoluteHtml = convertRelativeToAbsolute(html, 'http://localhost:1990/');
            expect(absoluteHtml).to.equal('<div><img src="http://localhost:1990/myimage.gif" /></div>');
        });

        it('should not replace image paths that start with //', () => {
            const html = '<div><img src="//www.someserver.com/myimage.gif" /></div>';
            const absoluteHtml = convertRelativeToAbsolute(html, 'http://localhost:1990/');
            expect(absoluteHtml).to.equal('<div><img src="//www.someserver.com/myimage.gif" /></div>');
        });

        it('should not replace image paths that start with http://', () => {
            const html = '<div><img src="http://www.someserver.com/myimage.gif" /></div>';
            const absoluteHtml = convertRelativeToAbsolute(html, 'http://localhost:1990/');
            expect(absoluteHtml).to.equal('<div><img src="http://www.someserver.com/myimage.gif" /></div>');
        });

        it('should not replace image paths that start with https://', () => {
            const html = '<div><img src="https://www.someserver.com/myimage.gif" /></div>';
            const absoluteHtml = convertRelativeToAbsolute(html, 'http://localhost:1990/');
            expect(absoluteHtml).to.equal('<div><img src="https://www.someserver.com/myimage.gif" /></div>');
        });

        it('should not replace links that start with //', () => {
            const html = '<div><a href="//www.someserver.com/myimage.gif">Link</a></div>';
            const absoluteHtml = convertRelativeToAbsolute(html, 'http://localhost:1990/');
            expect(absoluteHtml).to.equal('<div><a href="//www.someserver.com/myimage.gif">Link</a></div>');
        });

        it('should not replace links that start with http://', () => {
            const html = '<div><a href="http://www.someserver.com/myimage.gif">Link</a></div>';
            const absoluteHtml = convertRelativeToAbsolute(html, 'http://localhost:1990/');
            expect(absoluteHtml).to.equal('<div><a href="http://www.someserver.com/myimage.gif">Link</a></div>');
        });

        it('should not replace links that start with https://', () => {
            const html = '<div><a href="https://www.someserver.com/myimage.gif">Link</a></div>';
            const absoluteHtml = convertRelativeToAbsolute(html, 'http://localhost:1990/');
            expect(absoluteHtml).to.equal('<div><a href="https://www.someserver.com/myimage.gif">Link</a></div>');
        });
    });
});
