import React, { Component, PropTypes } from 'react';

export default class SpaceList extends Component {
    render() {
        const { content, containerClass, itemClass, baseUrl } = this.props;
        return (
            <this.props.tagName className={containerClass}>
                {content.map(result => (
                    <li key={result.id} className={itemClass}>
                        <a href={`${baseUrl}/display/${result.key}`} target="_top">{result.name}</a>
                    </li>
                ))}
            </this.props.tagName>
        );
    }
}

SpaceList.defaultProps = {
    tagName: 'ol'
};

SpaceList.propTypes = {
    tagName: PropTypes.oneOf(['ul', 'ol']),
    containerClass: PropTypes.string,
    itemClass: PropTypes.string,
    content: PropTypes.array,
    baseUrl: PropTypes.string
};
