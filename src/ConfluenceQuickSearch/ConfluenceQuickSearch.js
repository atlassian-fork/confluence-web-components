import React, { Component, PropTypes } from 'react';

import QuickSearch from './QuickSearch';

import { xdm_e } from '../context';

// These items have no way to be navigated to using the Connect navigation API, so they get filtered out.
const BLACKLIST = ['admin-item'];

export default class ConfluenceQuickSearch extends Component {
    constructor() {
        super();
        this._getRecentPages = this._getRecentPages.bind(this);
        this._handleSearchQueryChange = this._handleSearchQueryChange.bind(this);
        this.state = {
            isLoading: false,
            searchQuery: '',
            searchResults: []
        };
    }
    _getRecentPages() {
        this.setState({
            isLoading:true
        });
        AP.require('request', request => {
            request({
                url: '/rest/api/content/search?expand=container,metadata.currentuser.viewed.lastSeen&cql=type%20in%20(page,blogpost)%20and%20id%20in%20recentlyViewedContent(%208,%200)',
                success: response => {
                    response = JSON.parse(response);

                    const baseUrl = response._links.base;

                    const recentPages = response.results.map(result => {
                        return {
                            id: result.id,
                            lastSeen: new Date(result.metadata.currentuser.viewed.lastSeen),
                            title: result.title,
                            space: result.container.name,
                            type: result.type,
                            url: `${baseUrl}${result._links.webui}`
                        };
                    }).sort((a, b) => a.lastSeen < b.lastSeen);

                    this.setState({
                        isLoading:false,
                        recentPages
                    });
                }
            });
        });
    }
    _handleSearchQueryChange(searchQuery) {
        this.setState({
            searchQuery
        });
        if (searchQuery.length > 1) {
            this.setState({
                isLoading: true
            });
            AP.require('request', request => {
                request({
                    url: '/rest/quicknav/1/search?query=' + searchQuery,
                    success: response => {
                        response = JSON.parse(response);

                        // Only include content types that we can navigate to
                        let searchResults = response.contentNameMatches.filter(group => {
                            return BLACKLIST.indexOf(group[0].className) === -1;
                        });

                        // Re-write to absolute URLs
                        searchResults = searchResults.map(group => {
                            return group.map(result => result = { ...result, href: `${xdm_e()}${result.href}` });
                        });

                        this.setState({
                            isLoading: false,
                            searchResults
                        });
                    }
                });
            });
        } else {
            this.setState({
                searchResults: []
            });
        }
    }
    //_handleContentItemSelect(event, targetName, context) {
    //    AP.require('navigator', navigator => navigator.go(targetName, context));
    //    event.preventDefault();
    //}
    _handleSearch(event, query) {
        AP.require('navigator', navigator => navigator.go('sitesearch', { query }));
        event.preventDefault();
    }
    render() {
        return (
            <QuickSearch
                onFocusInput={this._getRecentPages}
                //onSelect={this._handleContentItemSelect}
                onSearch={this._handleSearch}
                searchQuery={this.state.searchQuery}
                onChangeSearchQuery={this._handleSearchQueryChange}
                containerClass={this.props.containerClass}
                placeholder={this.props.placeholder}
                recentPages={this.state.recentPages}
                searchResults={this.state.searchResults}
                showLoadingIndicator={this.state.isLoading}
            />
        );
    }
}

ConfluenceQuickSearch.propTypes = {
    /**
     * A CSS class name that will be applied to the root DOM node.
     */
    containerClass: PropTypes.string,
    /**
     * Placeholder text to show in the search input field.
     */
    placeholder: PropTypes.string
};
