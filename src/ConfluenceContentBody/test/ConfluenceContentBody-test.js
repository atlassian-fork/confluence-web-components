import React from 'react';

import sinon, { spy, stub } from 'sinon';
import sinonChai from 'sinon-chai';
import { shallow, render } from 'enzyme';
import chai, { expect } from 'chai';
import chaiEnzyme from 'chai-enzyme';
import proxyquire from 'proxyquire';
import { Constants } from '../ConfluenceContentBody';

chai.use(chaiEnzyme());
chai.use(sinonChai);

const baseUrl = 'http://localhost:0000';
const contextStub = stub().returns(baseUrl);

function getMockAP(mockResponse) {
    const mockAP = {
        request: (args) => {
            args.success(JSON.stringify(mockResponse));
        }
    };

    sinon.spy(mockAP, 'request');

    return mockAP;
}

function getConfluenceContentBody(mockAP) {
    const requireMock = sinon.stub();
    requireMock.withArgs('request').callsArgWith(1, mockAP.request);

    return proxyquire('../ConfluenceContentBody', {
        '../context': {
            xdm_e: contextStub
        },
        '../facades/AP': {
            require: requireMock
        }
    }).default;
}

const body = '<html></html>';
const cssDependencies = 'someStuff';
const jsDependencies = ['someUrl'];

const mockResponse = {
    body: {
        export_view: {
            value: body,
            webresource: {
                tags: {
                    css: cssDependencies
                },
                uris: {
                    js: jsDependencies
                }
            }
        }
    },
    metadata: {
        spaFriendly:true
    }
};

describe('ConfluenceContentBody', () => {
    describe('render', () => {
        let contentId;
        let wrapper;
        let instance;

        beforeEach(() => {
            contentId = '1234';
        });

        it('Should make a rest-api call to the content api', () => {
            const mockAP = getMockAP(mockResponse);
            const ConfluenceContentBody = getConfluenceContentBody(mockAP);
            wrapper = shallow(<ConfluenceContentBody contentId={contentId}/>);
            expect(mockAP.request).to.have.been.calledWith(sinon.match({
                url: `/rest/api/content/${contentId}?expand=${Constants.EXPANSIONS}`,
                success: sinon.match.typeOf('function')
            }));
        });

        describe('While request is running', () => {
            it('Should show a div', () => {
                const mockAP = getMockAP(mockResponse);
                mockAP.request = function(args) {
                    setTimeout(() => mockAP.request(args), 50);
                };

                const ConfluenceContentBody = getConfluenceContentBody(mockAP);

                wrapper = shallow(<ConfluenceContentBody contentId={contentId}/>);
                expect(wrapper).to.have.descendants('div');
            });
        });

        describe('When the given contentId is macroFriendly', () => {
            beforeEach(() => {
                const ConfluenceContentBody = getConfluenceContentBody(getMockAP(mockResponse));
                wrapper = shallow(<ConfluenceContentBody contentId={contentId}/>);
                instance = wrapper.instance();
            });

            it('Should set isSPAFriendly to true', () => {
                expect(instance.state.content.isSPAFriendly).to.equal(true);
            });

            it('Should set hasRequested to false', () => {
                expect(instance.state.hasRequested).to.equal(true);
            });

            it('Should return a ContentBody with appropiate arguments', () => {
                wrapper.update();
                expect(wrapper.is('ContentBody')).to.equal(true);
                expect(wrapper).to.have.prop('content').deep.equal({
                    id: contentId,
                    isSPAFriendly: true,
                    body,
                    cssDependencies,
                    jsDependencies
                });
            });
        });

        describe('When the given contentId is NOT macroFriendly', () => {
            beforeEach(() => {
                const response = Object.assign({}, mockResponse, { metadata: { spaFriendly: false } });
                const ConfluenceContentBody = getConfluenceContentBody(getMockAP(response));
                wrapper = shallow(<ConfluenceContentBody contentId={contentId}/>);
                instance = wrapper.instance();
            });

            it('Should set isSPAFriendly to true', () => {
                expect(instance.state.content.isSPAFriendly).to.equal(false);
            });

            it('Should set hasRequested to false', () => {
                expect(instance.state.hasRequested).to.equal(true);
            });

            it('Should return a ContentBody with appropiate arguments', () => {
                wrapper.update();
                expect(wrapper.is('ContentBody')).to.equal(true);
                expect(wrapper).to.have.prop('content').deep.equal({
                    id: contentId,
                    isSPAFriendly: false,
                    body,
                    cssDependencies,
                    jsDependencies
                });
            });
        });
    });
});
